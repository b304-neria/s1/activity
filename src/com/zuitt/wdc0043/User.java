package com.zuitt.wdc0043;

import java.util.Scanner;

public class User {

    public static void main(String[] args){
        Scanner myobj = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = myobj.nextLine();
        System.out.println("Last Name:");
        String lastName = myobj.nextLine();
        System.out.println("First Subject Grade:");
        double firstSubject = myobj.nextDouble();
        System.out.println("Second Subject Grade:");
        double secondSubject = myobj.nextDouble();
        System.out.println("Third Subject Grade:");
        double thirdSubject = myobj.nextDouble();
        double average = Math.round((firstSubject+secondSubject+thirdSubject)/3.0);
        System.out.println("Good day, "+firstName+" "+lastName+".");
        System.out.println("Your grade average is: " + average);

    }
}
